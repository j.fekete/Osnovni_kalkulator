package com.unique.juraj.prvizadatakv20;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class uniqueActivity extends AppCompatActivity implements View.OnClickListener{
    private Button Zbroji,Oduzmi,Mnozi,Dijeli;
    private TextView rezultat;
    private EditText prvi,drugi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unique);
        init();
    }
    private void init() {
        Zbroji = (Button) findViewById(R.id.Zbroji);
        Oduzmi = (Button) findViewById(R.id.Oduzmi);
        Mnozi = (Button) findViewById(R.id.Mnozi);
        Dijeli = (Button) findViewById(R.id.Dijeli);
        prvi = (EditText) findViewById(R.id.prvi);
        drugi = (EditText) findViewById(R.id.drugi);
        rezultat = (TextView) findViewById(R.id.rezultat);

        Zbroji.setOnClickListener(this);
        Oduzmi.setOnClickListener(this);
        Mnozi.setOnClickListener(this);
        Dijeli.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String br1 = prvi.getText().toString();
        String br2 = drugi.getText().toString();
        switch(v.getId()){
            case R.id.Zbroji:
                int zbrajanje = Integer.parseInt(br1) + Integer.parseInt(br2);
                rezultat.setText(String.valueOf(zbrajanje));
                break;
            case R.id.Oduzmi:
                int oduzimanje = Integer.parseInt(br1) - Integer.parseInt(br2);
                rezultat.setText(String.valueOf(oduzimanje));
                break;
            case R.id.Mnozi:
                int mnozenje = Integer.parseInt(br1) * Integer.parseInt(br2);
                rezultat.setText(String.valueOf(mnozenje));
                break;
            case R.id.Dijeli:
                try{
                    int dijeljenje = Integer.parseInt(br1) / Integer.parseInt(br2);
                    rezultat.setText(String.valueOf(dijeljenje));

                }
                catch (Exception e){
                    rezultat.setText("Ne moze podijeliti ");
                }

                break;
        }
    }
}

